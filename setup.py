from setuptools import setup, find_namespace_packages

setup(
    name='communication.rfdog',
    version='0.1.0',
    description='Module for working with rfcat dongles',
    author='Invuls',
    packages=find_namespace_packages(),
    package_data={'isf.communication.rfdog.resources': ['*', '**/*']},
    zip_safe=False
)
