from isf.core import logger
import rflib
import time


class RFdog:
    device = None
    recv_timeout_ms = 1000
    modulation = ''

    def __init__(self, need_to_replug=False, modulation='ASK_OOK',
                 package_len=250, recv_timeout_ms=1000, modem_baudrate=19200,
                 crc=False, power=999, noise_lvl=1):
        logger.warning(
            "Sometimes you need to unplug&plug dongle to work with it!")
        if need_to_replug:
            logger.info("Please, reconnect dongle to PC (10 seconds)")
            time.sleep(10)

        try:
            self.device = rflib.RfCat()
            self.device.makePktFLEN(package_len)
        except Exception as e:
            logger.error("Error: {}".format(e))
            return

        if self.set_modulation(modulation) == -1:
            return
        self.device.setMdmDRate(modem_baudrate)
        if crc:
            self.device.setEnablePktCRC()
        if power == 999:
            self.device.setMaxPower()
        else:
            self.device.setPower(power)
        self.device.lowball(level=noise_lvl)
        self.recv_timeout_ms = recv_timeout_ms

        logger.info("Device chipnum: {}".format(self.device.chipnum))
        return

    def set_modulation(self, modulation):
        if modulation.upper() == 'ASK_OOK':
            self.device.setMdmModulation(rflib.MOD_ASK_OOK)
        elif modulation.upper() == '2FSK':
            self.device.setMdmModulation(rflib.MOD_2FSK)
        elif modulation.upper() == 'FORMAT_2_FSK':
            self.device.setMdmModulation(rflib.MOD_FORMAT_2_FSK)
        elif modulation.upper() == 'FORMAT_GFSK':
            self.device.setMdmModulation(rflib.MOD_FORMAT_GFSK)
        elif modulation.upper() == 'FORMAT_MSK':
            self.device.setMdmModulation(rflib.MOD_FORMAT_MSK)
        elif modulation.upper() == 'GFSK':
            self.device.setMdmModulation(rflib.MOD_GFSK)
        elif modulation.upper() == 'MSK':
            self.device.setMdmModulation(rflib.MOD_MSK)
        else:
            logger.error("Modulation not found!")
            return -1
        self.modulation = modulation.upper()
        return 0

    def recv(self, time_ms=10000, pkgs=10 ** 6, freq=433000000, filename='None',
             live_print=True):
        current_time = time.time()
        packages = []
        self.device.setFreq(freq)
        while (time.time() - current_time < time_ms * 0.001) and len(
                packages) < pkgs:
            try:
                data, timestamp = self.device.RFrecv(
                    timeout=self.recv_timeout_ms)
                if live_print:
                    print('Captured: ', data.hex())
                packages.append({'data': data, 'timestamp': timestamp})
            except Exception as e:
                logger.warning("Package capture error: {}".format(e))
        logger.info(
            "Captured {} packages in {} seconds!".format(len(packages), int(
                time.time() - current_time)))

        if filename != 'None':
            try:
                f = open('filename', 'w')
                s_array = [package['timestamp'] + ': ' + package['data'].hex()
                           for
                           package in packages]
                s = '\n'.join(s_array)
                f.write(s)
                f.close()
            except Exception as e:
                logger.error("Error while writing to file: {}".format(e))
        return packages

    def send(self, data='31323334', send_time=10 ** 6, pkgs=1, freq=433000000,
             delay=100,
             live_print=True):
        self.device.setFreq(freq)
        try:
            decoded_data = bytes.fromhex(data).decode('charmap')
        except Exception as e:
            logger.error("Can't decode hex data: {}".format(e))
            return
        start_time = time.time()
        self.device.RFxmit(decoded_data)
        if live_print:
            logger.info("Package №{} was sent!".format(1))
        counter = 1
        while (time.time() - start_time < send_time) and (counter < pkgs):
            time.sleep(delay * 0.001)
            self.device.RFxmit(decoded_data)
            if live_print:
                logger.info("Package №{} was sent!".format(counter))
            counter += 1
        if live_print:
            logger.info("All {} packages were sent!".format(pkgs))

        return

    def detect_modulation(self, freq=433000000, sniff_time=5000,
                          delay_time=5000, live_print=True):
        last_modulation = self.modulation
        modulations_list = ['ASK_OOK', '2FSK', 'FORMAT_2_FSK', 'FORMAT_GFSK',
                            'FORMAT_MSK', 'GFSK', 'MSK']
        result = {}
        for modulation in modulations_list:
            logger.info('Prepare to detect {} modulation ( {} ms )!'.format(
                modulation, delay_time))
            time.sleep(delay_time * 0.001)

            self.set_modulation(modulation)
            logger.info(
                'Starting to detect {} modulation for {} ms!'.format(modulation,
                                                                     sniff_time))
            data = self.recv(time_ms=sniff_time, pkgs=10 ** 6, freq=freq,
                             live_print=live_print)
            result[modulation] = data
            logger.info('Stopped!')
        logger.info('Recovering {} modulation'.format(last_modulation))
        self.set_modulation(last_modulation)
        return result

    def replay_attack(self, freq=433000000, sniff_time=10000,
                      repeat_time=10000):
        logger.info("Starting to capture packets for {} ms!".format(sniff_time))
        data = self.recv(time_ms=sniff_time, pkgs=10 ** 6, freq=freq,
                         filename='None', live_print=False)
        logger.info("Captured {} packages!".format(len(data)))
        logger.info("Starting to send data for {} ms!".format(repeat_time))
        start_time = time.time()
        while (time.time() - start_time < repeat_time * 0.001):
            for package in data:
                self.send(data=package['data'].hex(), send_time=10 ** 6, pkgs=1,
                          freq=freq, live_print=False)
        logger.info("Repeat attack was finished!")
        return
